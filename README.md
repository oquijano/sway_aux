# Hammock

Bash scripts and bindings to help you feel relaxed in the [Sway windows
manager](https://swaywm.org/).

Video introduction to hammock: [Hammock: Relax in Sway](https://fediverse.tv/w/6DmzXovwVie5MMHx3rNAV8)


## Dependencies 

[fuzzel](https://codeberg.org/dnkl/fuzzel) and
[jq](https://jqlang.github.io/jq/) are needed for hammock to work.


## Installation

To install you can copy and paste the following in your terminal

```
curl 'https://gitlab.com/oquijano/hammock/-/raw/master/install.sh?ref_type=heads&inline=false' | bash
```

This takes the necessary files from the master branch of this
repository, if you would like it to use a different branch, set and
export the environment variable HAMOCK_BRANCH with the name of the
branch you want to take the files from.

The installation script does the following does the following:

1. Creates the hammock folder `~/.config/sway/hammock`
2. Downloads and copies the files `menus` and `mouse_mode` to the
   hammock folder.
3. Creates the file `config` to the hammock folder. It contains the
   sway configuration for using hammock including all keyboard bindings.
4. Adds an include statement to the `config` file from 3. at the end
   of the sway configuration file `~/.config/` 
5. It creates the folder `~/.local/state/hammock`. It is used to store
   the file `kb_mouse_speed` mouse mode to store and retrieve the
   mouse speed.
6. Reloads sway in order to apply the changes.

If for some reason you need some modifications to your environment
variables you can add the modifications to the file `hamock_env` in
the hammock folder.


## Features 

In the following list `mod` represents the logo key in your keyboard.

* **Increase/decrease transparency:** Shortcuts: `mod+t` and `mod+Shift+t`.

* **Change desktop:** Shortcut: `mod+o`. Changes the focus to the next output.

* **Actions menu:** Shortcut: `mod+Shift+o`. Shows a fuzzel menu with
  the following options
  - Toggle waybar
  - Change the scale of an output
  - Change the resolution of an output
  - Rotate an output
  - Move a workspace to an output
  
* **Mouse mode:** Shortcut: `mod+Shift+m`. Starts the mouse mode,
  which allows to move the mouse using the keyboard. In the mouse
  mode, the following bindings can be used:
  - `j` Moves the mouse down
  - `k` Moves the mouse up
  - `h` Moves the mouse to the left
  - `l` Moves the mouse to the right
  - `u` Increases the distance the mouse travels with each move
  - `n` Decreases the distance the mouse travels with each move
  - `c` Left click
  - `r` right click
  - `d` drag the mouse (often you need to press it twice)
  - `mod+Shift+m` Go back to the default mode
  - `Return` Go back to the default mode
  - `Escape` Go back to the default mode
  
All these keyboard shortcuts can be changed in the configuration file
`~/.config/sway/hammock/config`.
