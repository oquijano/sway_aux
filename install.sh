# Hammock - Convenient components for the sway windows manager.
# Copyright (C) 2024 Oscar Alberto Quijano Xacur

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

export VERSION=1.1

export SWAY_CONFIG_FOLDER=${XDG_CONFIG_HOME:-~/.config}/sway
export SWAY_CONFIG_PATH=$SWAY_CONFIG_FOLDER/config
export HAMMOCK_CONFIG_FOLDER="${SWAY_CONFIG_FOLDER}/hammock"
export STATE_FOLDER=${XDG_STATE_HOME:-$HOME/.local/state}
export HAMMOCK_STATE=${STATE_FOLDER}/hammock

export ENV_FILE=${HAMMOCK_CONFIG_FOLDER}/hammock_env
export VERSION_FILE=${HAMMOCK_CONFIG_FOLDER}/VERSION
export HAMMOCK_CONFIG_PATH=${HAMMOCK_CONFIG_FOLDER}/config

export BRANCH=${HAMOCK_BRANCH:-master}


create_status_folder(){
    mkdir -p "${HAMMOCK_STATE}" 
}

gitlab_file_url(){
    local file="$1"
    echo "https://gitlab.com/oquijano/hammock/-/raw/${BRANCH}/${file}?ref_type=heads&inline=false"
}

install_menus_script(){
    local menus_file="${HAMMOCK_CONFIG_FOLDER}/menus"
    local download_url="$(gitlab_file_url menus)"
    curl -s -o "$menus_file" "$download_url"
    chmod u+x "$menus_file"
}

install_mouse_mode(){
    local mouse_file="${HAMMOCK_CONFIG_FOLDER}/mouse_mode"
    local download_url="$(gitlab_file_url mouse_mode)"
    curl -s -o "${mouse_file}" "$download_url"
    chmod u+x "${mouse_file}"
}

create_env_file(){
    if [ ! -f "$ENV_FILE" ]
    then
	cat <<EOF > "$ENV_FILE"
# Write or edit environemnt variables so that the commands
# jq, fuzzel and realpath are in PATH

export HAMMOCK_STATE=$HAMMOCK_STATE
EOF
    fi
}

write_version(){
    echo $VERSION > "$VERSION_FILE"
}

create_config_file(){
    if [ ! -f "$HAMMOCK_CONFIG_PATH" ]
    then	
	cat <<EOF > "$HAMMOCK_CONFIG_PATH"
set \$hammock_path $HAMMOCK_CONFIG_FOLDER

## Show actions menu
bindsym \$mod+Shift+o exec \$hammock_path/menus -o
## Focus next output
bindsym \$mod+o exec \$hammock_path/menus -s

## Increase transparency     
bindsym \$mod+t opacity minus 0.05
## Decrease transparency     
bindsym \$mod+Shift+t opacity plus 0.05



Mode mouse {
     bindsym \$mod+Shift+m mode default
     ## mouse down
     bindsym j exec \$hammock_path/mouse_mode -j
     ## mouse up
     bindsym k exec \$hammock_path/mouse_mode -k
     ## mouse left
     bindsym h exec \$hammock_path/mouse_mode -h
     ## mouse right
     bindsym l exec \$hammock_path/mouse_mode -l

     ## Increase speed
     bindsym u exec \$hammock_path/mouse_mode -u
     ## Decrease speed
     bindsym n exec \$hammock_path/mouse_mode -d

     ## Drag mouse (left button)
     bindsym d seat - cursor press button1

     ## Left click
     bindsym --no-repeat c seat - cursor press button1
     bindsym --release c seat - cursor release button1

     ## Right click
     bindsym --no-repeat r seat - cursor press button3
     bindsym --release r seat - cursor release button3

     bindsym Return mode "default"
     bindsym Escape mode "default"
}

bindsym \$mod+Shift+m mode mouse

EOF
    fi
}

check_hammock_in_config(){
    grep '\-\-Hammock starts\-\-' "$SWAY_CONFIG_PATH"
}

## Add comments to prevent adding hammocks config more than once.

include_hammock_in_config(){
    if [ ! "$(check_hammock_in_config)" ]
    then
	cat <<EOF >> "$SWAY_CONFIG_PATH"

## --Hammock starts--
include $HAMMOCK_CONFIG_PATH
## --Hammock ends--
EOF
    fi
    
}


install(){
    echo "Installing hammock."
    echo "Branch: ${BRANCH}"
    echo "Creating state file: ${HAMMOCK_STATE}."
    mkdir -p "$HAMMOCK_CONFIG_FOLDER"
    create_env_file
    echo "Downloading menus script."
    install_menus_script
    echo "Downloading mouse mode script."
    install_mouse_mode
    echo "Creating environment file: ${ENV_FILE}"
    create_env_file
    echo "Creating config file: ${HAMMOCK_CONFIG_PATH}"
    create_config_file
    echo "Adding hammock include to sway config file."
    include_hammock_in_config
    echo "Creating status folder: ${HAMMOCK_STATE}"
    create_status_folder
    write_version
    echo "Reloading sway"
    swaymsg reload
    echo "hammock has been installed!"    
}

install
